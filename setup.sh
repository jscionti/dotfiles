#!/bin/bash
set -e
cd "$(dirname "$0")"

# prerequisites {{{
if [[ "$1" != "--skip-install" ]]; then
    echo "[*] Installing prerequisites..."
    sudo apt install -y curl wget git zsh python3-venv npm ripgrep fzf gdb \
        meld npm tmux xsel
    if [[ "$1" == "--install-only" ]]; then exit 1; fi
fi
# }}}

# zsh {{{
echo "[*] Configuring zsh..."
#git clone --depth=1 https://github.com/romkatv/powerlevel10k.git \
#    ~/.powerlevel10k
if [ -f ~/.zshrc ]; then
    mv ~/.zshrc ~/.zshrc.bak
fi
cp -f zshrc ~/.zshrc
# }}}

# neovim {{{
if [[ "$(uname -m)" == "x86_64" ]]; then
    echo "[*] Installing neovim to '$HOME/.local/bin'..."
    if ! [ -d $HOME/.local/bin ]; then
        mkdir -p $HOME/.local/bin
    fi
    if [[ ! "$PATH" == *"$HOME/.local/bin"* ]]; then
        export PATH="$HOME/.local/bin:$PATH"
    fi
    curl -sL \
        https://github.com/neovim/neovim/releases/latest/download/nvim-linux64.tar.gz | \
        tar -xz -C $HOME/.local/bin/
    ln -s $HOME/.local/bin/nvim-linux64/bin/nvim $HOME/.local/bin/nvim

    mkdir -p ${XDG_CONFIG_HOME:=$HOME/.config}/nvim
    cp -r nvim/* "${XDG_CONFIG_HOME:-$HOME/.config}"/nvim/
    git clone --depth 1 https://github.com/wbthomason/packer.nvim \
      ~/.local/share/nvim/site/pack/packer/start/packer.nvim
    nvim --headless -c 'autocmd User PackerComplete quitall' -c 'PackerSync' \
        2>/dev/null
    nvim --headless -c PackerSync -c 'sleep 5' -c qa 2>/dev/null
    git config --global core.editor "nvim"
fi
# }}}

# vim {{{
if [[ "$(uname -m)" == "aarch64" ]]; then
    echo "[*] Installing vim..."
    sudo apt install -y vim-gtk3
    echo "[*] Configuring vim..."
    cp -f vimrc ~/.vimrc
    curl -fLso ~/.vim/autoload/plug.vim --create-dirs \
        https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
    cp -r vim-colors ~/.vim/colors
    git config --global core.editor "vim"
    npm install write-good
    export pandoc_name=$(curl -s https://api.github.com/repos/jgm/pandoc/releases/latest | \
         grep -e "name.*arm64.deb" | cut -d '"' -f 4)
    export pandoc_url=$(curl -s https://api.github.com/repos/jgm/pandoc/releases/latest | \
         grep -e "browser_download_url.*arm64.deb" | cut -d '"' -f 4)
    wget $pandoc_url && sudo dpkg -i $pandoc_name && rm $pandoc_name
    vim +PlugInstall +qall
fi
# }}}

# gdb {{{
echo "[*] Configuring gdb..."
cp -f gdbinit ~/.gdbinit
wget -O ~/.gdbinit-gef.py -q https://gef.blah.cat/py
echo source ~/.gdbinit-gef.py >> ~/.gdbinit
# }}}

# git {{{
echo "[*] Configuring git..."
git config --global merge.tool "meld"
# }}}

# tmux {{{
echo "[*] Configuring tmux..."
cp -f tmux.conf ~/.tmux.conf
git clone https://github.com/tmux-plugins/tpm ~/.tmux/plugins/tpm
mkdir -p ~/.local/share/fonts
wget https://github.com/ryanoasis/nerd-fonts/releases/download/v3.2.1/FiraCode.zip
sudo unzip -j FiraCode.zip "*.ttf" -d /usr/local/share/fonts
fc-cache -f -v
# }}}

echo "==="
echo "=== Next steps:"
echo "=== Change the font in your terminal to FiraCode"
echo "=== Run: chsh -s $(which zsh) && $(which zsh)"
echo "=== Open: tmux and install plugins with Ctrl+s + I"
echo "==="
echo ""
echo "[*] Done!"

# vim:foldmethod=marker:foldlevel=0
