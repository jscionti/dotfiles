" General {{{
" Sets how many lines of history VIM has to remember
set history=500

" Set to auto read when a file is changed from the outside
set autoread

" Enable use of the mouse for all modes
set mouse=a

" Set 'nocompatible' to ward off unexpected things that your distro might
" have made, as well as sanely reset options when re-sourcing .vimrc
set nocompatible

" Show partial commands in the last line of the screen
set showcmd

" Turn persistent undo on
try
    set undodir=~/.vim_runtime/temp_dirs/undodir
    set undofile
catch
endtry

" Bash like keys for the command line
cnoremap <C-A>		<Home>
cnoremap <C-E>		<End>
cnoremap <C-K>		<C-U>

cnoremap <C-P> <Up>
cnoremap <C-N> <Down>
" }}}

" User Interface {{{
" Set 7 lines to the cursor - when moving vertically using j/k
set so=7

" Better command-line completion
set wildmenu

" Ignore compiled files
set wildignore=*.o,*~,*.pyc
if has("win16") || has("win32")
    set wildignore+=.git\*,.hg\*,.svn\*
else
    set wildignore+=*/.git/*,*/.hg/*,*/.svn/*,*/.DS_Store
endif

" Display the cursor position on the last line of the screen or in the status
" line of a window
set ruler

" Allows you to re-use the same window and switch from an unsaved buffer without
" saving it first. Also allows you to keep an undo history for multiple files
" when re-using the same window in this way. Note that using persistent undo
" also lets you undo in multiple files even in the same window, but is less
" efficient and is actually designed for keeping undo history after closing Vim
" entirely. Vim will complain if you try to quit without saving, and swap files
" will keep you safe if your computer crashes.
set hidden

" Configure backspace so it acts as it should act
set backspace=eol,start,indent
set whichwrap+=<,>,h,l

" Use case insensitive search, except when using capital letters
set ignorecase
set smartcase

" Highlight searches
set hlsearch
" Map <C-n> (redraw screen) to also turn off search highlighting until the
" next search
nnoremap <C-n> :nohl<CR><C-L>

" Makes search act like search in modern browsers
set incsearch

" Don't redraw while executing macros (good performance config)
set lazyredraw

" For regular expressions turn magic on
set magic

" Show matching brackets when text indicator is over them
set showmatch
" How many tenths of a second to blink when matching brackets
set mat=2

" Show line numbers
set number
" Highlight current line
set cursorline

" Disable scrollbars
set guioptions-=r
set guioptions-=R
set guioptions-=l
set guioptions-=L

" No annoying sound on errors
set noerrorbells
set novisualbell
set vb t_vb=
set tm=500
" }}}

" Colours and Fonts {{{
" Enable syntax highlighting
syntax on

" Set colourscheme
colorscheme jellybeans
hi NonText ctermbg=none
hi Normal guibg=NONE ctermbg=NONE

" Enable 256 colors palette in Gnome Terminal
if $COLORTERM == 'gnome-terminal'
    set t_Co=256
endif

" Set utf8 as standard encoding and en_US as the standard language
set encoding=utf8

" Use Unix as the standard file type
set ffs=unix,dos,mac
" }}}

" Files, backups and undo {{{
" Turn backup off, since most stuff is in SVN, git et.c anyway...
set nobackup
set nowb
set noswapfile
" }}}

" Text, tabs and indents {{{
" Attempt to determine the type of a file based on its name and possibly its
" contents. Use this to allow intelligent auto-indenting for each filetype,
" and for plugins that are filetype specific.
filetype indent plugin on

" Use spaces instead of tabs
set expandtab

" Be smart when using tabs ;)
set smarttab

" 1 tab == 4 spaces
set shiftwidth=4
set tabstop=4

set ai "Auto indent
set si "Smart indent
set wrap "Wrap lines

" Configure folding
set foldenable " enable folding
set foldlevelstart=10 " open most folds by default
set foldnestmax=10 " limit the number of nested folds
set foldmethod=indent " fold based on indent level
" open/close with space
nnoremap <space> za
" }}}

" Moving around, tabs and buffers {{{
" Let 'tl' toggle between this and the last accessed tab
let g:lasttab = 1
nmap <Leader>tl :exe "tabn ".g:lasttab<CR>
au TabLeave * let g:lasttab = tabpagenr()
" }}}

" Mappings {{{

" Move vertically by visual line
nnoremap j gj
nnoremap k gk

" In insert or command mode, move normally by using Ctrl
inoremap <C-h> <Left>
inoremap <C-j> <Down>
inoremap <C-k> <Up>
inoremap <C-l> <Right>
cnoremap <C-h> <Left>
cnoremap <C-j> <Down>
cnoremap <C-k> <Up>
cnoremap <C-l> <Right>

if has("mac") || has("macunix")
  nmap <D-j> <M-j>
  nmap <D-k> <M-k>
  vmap <D-j> <M-j>
  vmap <D-k> <M-k>
endif

" Delete trailing white space on save, useful for some filetypes ;)
fun! CleanExtraSpaces()
    let save_cursor = getpos(".")
    let old_query = getreg('/')
    silent! %s/\s\+$//e
    call setpos('.', save_cursor)
    call setreg('/', old_query)
endfun

if has("autocmd")
    autocmd BufWritePre *.txt,*.js,*.py,*.wiki,*.sh,*.coffee :call CleanExtraSpaces()
endif
" }}}

" Spell checking {{{
" Pressing ,ss will toggle and untoggle spell checking
map <leader>ss :setlocal spell!<cr>

" Shortcuts using <leader>
map <leader>sn ]s
map <leader>sp [s
map <leader>sa zg
map <leader>s? z=
" }}}

" Plugins {{{
call plug#begin('~/.vim/plugged')
Plug 'christoomey/vim-tmux-navigator'
Plug 'airblade/vim-gitgutter'
Plug 'rbong/vim-crystalline'
Plug 'dense-analysis/ale'
Plug 'maxboisvert/vim-simple-complete'
Plug 'https://github.com/preservim/nerdtree', { 'on': 'NERDTreeToggle' }
Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
Plug 'junegunn/fzf.vim'
Plug 'https://github.com/kshenoy/vim-signature'
call plug#end()

" GitGutter
highlight GitGutterAdd    guifg=#009900 ctermfg=2
highlight GitGutterChange guifg=#bbbb00 ctermfg=3
highlight GitGutterDelete guifg=#ff2222 ctermfg=1
set updatetime=100 " shows instant gitgutter changes

" Vim-Crystalline
function! StatusLine(...)
  return crystalline#mode() . crystalline#right_mode_sep('')
        \ . ' %f%h%w%m%r ' . crystalline#right_sep('', 'Fill') . '%='
        \ . crystalline#left_sep('', 'Fill')
        \ . ' %{&ft}[%{&enc}][%{&ffs}] %l/%L %c%V %P '
endfunction
let g:crystalline_theme = 'jellybeans'
set laststatus=2

" Ale

let g:ale_sign_error             = '✘'
let g:ale_sign_warning           = '⚠'
highlight ALEErrorSign ctermbg   =NONE ctermfg=red
highlight ALEWarningSign ctermbg =NONE ctermfg=yellow
let g:ale_linters_explicit       = 1
let g:ale_lint_on_text_changed   = 'never'
let g:ale_lint_on_enter          = 0
let g:ale_lint_on_save           = 1
let g:ale_fix_on_save            = 1
let g:ale_linters = { 'markdown': ['writegood'] }
let g:ale_fixers  = { '*': ['remove_trailing_lines',
                    \       'trim_whitespace'],
                    \ 'markdown': ['pandoc'],
                    \ 'c': ['clang-format'],
                    \ 'python': ['black'] }

" Vim Simple Complete
set complete-=t
set complete-=i

" NERDTree

nnoremap <C-t> :NERDTreeToggle<CR>
let NERDTreeQuitOnOpen=1

" Exit Vim if NERDTree is the only window remaining in the only tab.
autocmd BufEnter * if tabpagenr('$') == 1 && winnr('$') == 1 && exists('b:NERDTree') && b:NERDTree.isTabTree() | call feedkeys(":quit\<CR>:\<BS>") | endif

" Vim Tmux Navigator

let g:tmux_navigator_no_mappings = 1
let g:tmux_navigator_save_on_switch = 2

nnoremap <silent> <C-l> :<C-U>TmuxNavigateLeft<cr>
nnoremap <silent> <C-j> :<C-U>TmuxNavigateDown<cr>
nnoremap <silent> <C-k> :<C-U>TmuxNavigateUp<cr>
nnoremap <silent> <C-h> :<C-U>TmuxNavigateRight<cr>

" fzf

nnoremap <C-f> :Files<cr>
nnoremap <C-g> :RG<cr>

" }}}

set modelines=1

" vim:foldmethod=marker:foldlevel=0
