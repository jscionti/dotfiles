# dotfiles

## Linux Install

```bash
TEMP=$(mktemp -d) && \
  git clone https://gitlab.com/jscionti/dotfiles $TEMP/dotfiles && \
  cd $TEMP/dotfiles && ./setup.sh
```

## Windows Install

```powershell
echo "IEX(New-Object Net.WebClient).DownloadString('https://gitlab.com/jscionti/dotfiles/winstall.ps1')" | powershell -NoProfile -ExecutionPolicy bypass
```
