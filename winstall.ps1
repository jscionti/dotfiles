# VMWare Prerequisite: VM Settings > Hardware > Processors > Virtualise Intel VT-x/EPT

$command = "powershell.exe -ExecutionPolicy bypass -File $PSCommandPath"

if (!$args -or $args[0] -eq "stage1_update") {
    Set-ExecutionPolicy -Scope Process -ExecutionPolicy bypass
    Install-Module -Name PSWindowsUpdate -Confirm -Force
    Import-Module PSWindowsUpdate
    Get-WindowsUpdate -Install -AcceptAll -IgnoreReboot
    Set-ItemProperty "HKLM:\Software\Microsoft\Windows\CurrentVersion\RunOnce" -Name "ContinueSetup" -Value "$command stage2_update_again"
    Restart-Computer
} elseif ($args[0] -eq "stage2_update_again" {
    Import-Module PSWindowsUpdate
    Get-WindowsUpdate -Install -AcceptAll -IgnoreReboot
    Set-ItemProperty "HKLM:\Software\Microsoft\Windows\CurrentVersion\RunOnce" -Name "ContinueSetup" -Value "$command stage3_install_wsl"
    Restart-Computer
} elseif ($args[0] -eq "stage3_install_wsl" {
    DISM /Online /Enable-Feature /All /NoRestart /FeatureName:Microsoft-Windows-Subsystem-Linux
    DISM /Online /Enable-Feature /All /NoRestart /FeatureName:VirtualMachinePlatform
    DISM /Online /Enable-Feature /All /NoRestart /FeatureName:Microsoft-Hyper-V
    bcfedit /set hypervisorlaunchtype auto
    wsl --install -d debian
    Set-ItemProperty "HKLM:\Software\Microsoft\Windows\CurrentVersion\RunOnce" -Name "ContinueSetup" -Value "$command stage4_install_apps"
    Restart-Computer
} elseif ($args[0] -eq "stage4_install_apps" {
    Invoke-WebRequest -Uri https://aka.ms/getwinget -OutFile $env:TEMP\Microsoft.DesktopAppInstaller.msixbundle
    Invoke-WebRequest -Uri https://aka.ms/Microsoft.VCLibs.x64.14.00.Desktop.appx -OutFile $env:TEMP\Microsoft.VCLibs.x64.14.00.Desktop.appx
    Invoke-WebRequest -Uri https://github.com/microsoft/microsoft-ui-xml/releases/download/v2.7.3/Microsoft.UI.Xaml.2.7.x64.appx -OutFile $env:TEMP\Microsoft.UI.Xaml.2.7.x64.appx
    Add-AppxPackage $enf:TEMP\Microsoft.DesktopAppInstaller.msixbundle
    Add-AppxPackage $enf:TEMP\Microsoft.VCLibs.x64.14.00.Desktop.appx
    Add-AppxPackage $enf:TEMP\Microsoft.UI.Xaml.2.7.x64.appx

    winget install -e --id=Microsoft.WindowsTerminal --accept-source-agreements
    winget install -e --id=Mozilla.Firefox.DeveloperEdition
    winget install -e --id=Docker.DockerDesktop
    winget install -e --id=Python.Python.3.12
    
    mkdir C:\tools
    Invoke-WebRequest -Uri https://download.sysinternals.com/files/SysinternalsSuite.zip -Outfile C:\tools\SysinternalsSuite.zip
    Expand-Archive -Path C:\tools\SysinternalsSuite.zip -DestinationPath C:\tools\Sysinternals
    Remove-Item C:\tools\SysinternalsSuite.zip

    cmd.exe /c $PsScriptRoot\privacy.bat

    wsl -d debian -u root -e sh -c 'apt update && apt upgrade -y && apt install git curl -y'
    wsl -d debian -e sh -c @'
        . ~/.profile
        cd $(mktemp -d)
        git clone https://gitlab.com/jscionti.dotfiles
        cd dotfiles && ./setup.sh
    '@
    wsl -d debian -e sh -c 'chsh -s /usr/bin/zsh'

    Restart-Computer
}
