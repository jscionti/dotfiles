# Configure powerlevel10k
if [[ -r "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh" ]]; then
  source "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh"
fi
source ~/.powerlevel10k/powerlevel10k.zsh-theme
[[ ! -f ~/.p10k.zsh ]] || source ~/.p10k.zsh

# Configure history
export HISTSIZE=2000
export HISTFILE="$HOME/.zsh_history"
export SAVEHIST=$HISTSIZE
setopt hist_ignore_all_dups
setopt hist_ignore_space

# Aliases
alias v="vim"
alias grep="grep --color"
alias fzf-cd='cd $(dirname $(fzf)))'
alias gs="git status"
alias ga="git add"
alias gc="git commit"
alias gca="git commit --amend"
alias gcm="git commit -m"
alias gpsh="git push"
alias gpshsu="git push --set-upstream"
alias gpshf="git push --force-with-lease"
alias gpu="git pull"
alias gfa="git fetch --all"
alias gc="git checkout"
alias gr="git rebase"
alias grc="git rebase --continue"
alias gra="git rebase --abort"
alias gmt="git mergetool"

# Fixes
autoload -Uz compinit && compinit
zstyle ':completion:*' matcher-list 'm:{a-z}={A-Za-z}'

bindkey "^[[1;5C" forward-word
bindkey "^[[1;5D" backward-word
bindkey "^[[H"    beginning-of-line
bindkey "^[[F"    end-of-line
bindkey "\e[3~"   delete-char

# Environment variables
if [[ ! $PATH =~ "$HOME/.local/bin" ]]; then
    export PATH=$HOME/.local/bin:$PATH
fi
export XDG_CONFIG_HOME=$HOME/.config
export TERM=screen-256color

if [ -e "$HOME/.local/bin/nvim" ]; then
    alias n="nvim"
    export NVM_DIR="$HOME/.config/nvm"
    [ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
    [ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion
fi
