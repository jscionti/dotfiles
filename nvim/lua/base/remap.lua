vim.g.mapleader = " "
vim.keymap.set("n", "<leader><Esc>", vim.cmd.Ex)

-- Handle word wrapping
vim.keymap.set('n', 'k', "v:count == 0 ? 'gk' : 'k'", {expr=true, silent=true})
vim.keymap.set('n', 'j', "v:count == 0 ? 'gj' : 'j'", {expr=true, silent=true})
vim.o.whichwrap="b,h,l,<,>"

-- Move a line of text using Ctrl + [jk]
vim.keymap.set('n', '<C-k>', 'mz:m-2<cr>`z', {silent=true})
vim.keymap.set('n', '<C-j>', 'mz:m+<cr>`z', {silent=true})

-- Move highlighted lines
vim.keymap.set('v', 'J', ":m '>+1<CR>gv=gv")
vim.keymap.set('v', 'K', ":m '<-2<CR>gv=gv")

-- Move normally using Ctrl when in Insert or Command mode
vim.keymap.set('i', '<C-h>', '<Left>', {silent=true})
vim.keymap.set('i', '<C-j>', '<Down>', {silent=true})
vim.keymap.set('i', '<C-k>', '<Up>', {silent=true})
vim.keymap.set('i', '<C-l>', '<Right>', {silent=true})
vim.keymap.set('c', '<C-h>', '<Left>', {silent=true})
vim.keymap.set('c', '<C-j>', '<Down>', {silent=true})
vim.keymap.set('c', '<C-k>', '<Up>', {silent=true})
vim.keymap.set('c', '<C-l>', '<Right>', {silent=true})

-- Keep cursor in the middle of the screen after page up/down and searching
vim.keymap.set('n','<C-d>','<C-d>zz')
vim.keymap.set('n','<C-u>','<C-u>zz')
vim.keymap.set('n','n','nzzzv')
vim.keymap.set('n','N','Nzzzv')

-- Leader yank to clipboard
vim.keymap.set("n", "<leader>y", "\"+y")
vim.keymap.set("v", "<leader>y", "\"+y")
vim.keymap.set("n", "<leader>Y", "\"+Y")

-- Leader delete to void
vim.keymap.set("n", "<leader>d", "\"_d")
vim.keymap.set("v", "<leader>d", "\"_d")

-- Spell checking
vim.keymap.set('n','<leader>ss',':setlocal spell!<cr>', {silent=true})
vim.keymap.set('n','<leader>sn',':]s',{silent=true}) -- Next misspelt word
vim.keymap.set('n','<leader>sp',':[s',{silent=true}) -- Previous misspelt word
vim.keymap.set('n','<leader>sa',':zg',{silent=true}) -- Add to dictionary
vim.keymap.set('n','<leader>s?',':z=',{silent=true}) -- Show correction options

-- QOL
vim.keymap.set("n", "Q", "<nop>") -- Disable Q
vim.keymap.set("n", "<leader>s",  -- Replace word at cursor
    [[:%s/\<<C-r><C-w>\>/<C-r><C-w>/gI<Left><Left><Left>]])
