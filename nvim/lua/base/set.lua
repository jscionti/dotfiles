-- Set vim options, see `:h vim.o`

-- Configure folding
vim.o.foldenable=true     -- Enable folding
vim.o.foldlevelstart=10   -- Open most folds by default
vim.o.foldnestmax=10      -- Limit the number of nested folds
vim.o.foldmethod='indent' -- Fold based on indent level
vim.keymap.set('n','<leader><space>','za',{silent=true}) -- Toggle fold

-- Case-insensitive searching unless \C or capital in search
vim.o.ignorecase=true
vim.o.smartcase=true

-- Decrease update time
vim.o.updatetime=50
vim.o.timeoutlen=200

-- Turn off backup
vim.o.backup=false
vim.o.swapfile=false
vim.o.writebackup=false

-- Set completeopt to have a better completion experience
vim.o.completeopt='menuone,noselect'

-- Enable terminal colors
vim.o.termguicolors=true

-- Highlight long lines
vim.o.colorcolumn="80"
vim.cmd[[highlight OverLength ctermbg=red ctermfg=white guibg=red]]
vim.cmd[[match OverLength /\%81v.\+/]]

-- Spaces > Tabs
vim.o.expandtab=true
vim.o.shiftwidth=4
vim.o.softtabstop = 4
vim.o.tabstop=4

-- Indentation
vim.o.autoindent=true
vim.o.breakindent=true
vim.o.smartindent=true
vim.o.wrap=true

-- Configure search
vim.o.hlsearch=true    -- Highlight search results
vim.o.incsearch = true -- Show search results incrementally

-- Configure the cursor
vim.o.cursorline=true -- Highlight cursor line
vim.o.guicursor = ""  -- Use block cursor
vim.o.mouse='a'       -- Enable mouse mode

-- File controls
vim.o.autoread=true      -- Auto read when a file is changed
vim.o.ffs='unix,dos,mac' -- Use Unix as the standard file type
vim.o.undofile=true      -- Save undo history
vim.o.spell=true         -- Enable spell check

-- Window scoped, see `:h vim.w`
vim.wo.number=true      -- Show line numbers
vim.wo.signcolumn='yes' -- Show signcolumn
vim.wo.scrolloff = 5    -- Always show the top or bottom 5 lines
