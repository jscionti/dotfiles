vim.keymap.set("n", "<leader>gs", function() vim.cmd.Git("status") end);
vim.keymap.set("n", "<leader>ga", function() vim.cmd.Git("add %") end);
