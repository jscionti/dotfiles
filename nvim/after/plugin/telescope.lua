local builtin = require('telescope.builtin')
vim.keymap.set('n', '<leader>sf', builtin.find_files, {}) -- [S]earch [F]iles
vim.keymap.set('n', '<leader>sg', builtin.git_files, {})  -- [S]earch [G]it
vim.keymap.set('n', '<leader>gs',                         -- [G]rep [S]earch
    function()
        builtin.grep_string({ search = vim.fn.input("grep: ") })
    end)
